import React from "react";
import logo from "./logo.svg";
import "./App.css";
import DemoProps from "./DemoProps/DemoProps";
import Ex_Todos from "./Todos/Ex_Todos";
import ReduxNumber from "./ReduxNumber/ReduxNumber";

function App() {
  return (
    <div className="App">
      {/* <DemoProps /> */}
      {/* <Ex_Todos /> */}
      <ReduxNumber />
    </div>
  );
}

export default App;
