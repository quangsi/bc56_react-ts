//  action : tang , giam

import { NumberType } from "./redux/contant/number";

interface I_TangAction {
  type: NumberType.TANG;
  payload: number;
}

interface I_GiamAction {
  type: NumberType.GIAM;
  payload: number;
}

export type ActionType = I_TangAction | I_GiamAction;
