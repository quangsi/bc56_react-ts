import { combineReducers } from "redux";
import { numberReducer } from "./number";

export let rootReducer = combineReducers({
  numberReducer,
});
