import { ActionType } from "../../interface";
import { NumberType } from "../contant/number";

interface I_InitialState {
  soLuong: number;
}

let initialState: I_InitialState = {
  soLuong: 100,
};

export const numberReducer = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case NumberType.TANG: {
      state.soLuong += action.payload;
      return { ...state };
    }
    case NumberType.GIAM: {
      state.soLuong -= action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
