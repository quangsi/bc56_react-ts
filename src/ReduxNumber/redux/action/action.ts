import { NumberType } from "../contant/number";

export const tangSoLuongAction = (value: number) => {
  return {
    type: NumberType.TANG,
    payload: value,
  };
};
