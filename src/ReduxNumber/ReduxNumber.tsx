import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "..";
import { tangSoLuongAction } from "./redux/action/action";

export default function ReduxNumber() {
  let number = useSelector((state: RootState) => state.numberReducer.soLuong);
  let dispatch = useDispatch();
  let handleTang = (number: number) => {
    // dispatch({});
    dispatch(tangSoLuongAction(number));
  };
  return (
    <div>
      <button className="btn btn-dark">-</button>
      <strong className="mx-5">{number}</strong>
      <button
        onClick={() => {
          handleTang(10);
        }}
        className=" btn btn-danger"
      >
        +
      </button>
    </div>
  );
}
