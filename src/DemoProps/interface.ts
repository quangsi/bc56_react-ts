export interface I_Profile {
  id: number;
  name: string;
  address: string;
}

export interface I_PropsCard {
  info: I_Profile;
}
