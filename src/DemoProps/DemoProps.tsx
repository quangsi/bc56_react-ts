import React from "react";
import Card from "./Card";
import { I_Profile } from "./interface";

export default function DemoProps() {
  let profile: I_Profile = {
    id: 111,
    name: "aclice",
    address: "112 cao thắng",
  };
  return (
    <div>
      <Card info={profile} />
    </div>
  );
}
