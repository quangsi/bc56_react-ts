import React from "react";
import { I_PropsCard } from "./interface";

export default function Card(props: I_PropsCard) {
  console.log(props.info);

  return (
    <div>
      <h1>Id: {props.info.id}</h1>
      <h1>Name: {props.info.name}</h1>
      <h1>Address: {props.info.address}</h1>
    </div>
  );
}
