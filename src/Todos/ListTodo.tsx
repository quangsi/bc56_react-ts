import React from "react";
import { I_PropsList } from "./interface";
import ItemTodo from "./ItemTodo";

export default function ListTodo(props: I_PropsList) {
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>IsCompleted</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {/* map ra thẻ tr */}
          {props.todos.map((item, index) => {
            return (
              <ItemTodo
                handleToggleCheck={props.handleToggleCheck}
                handleDelete={props.handleDelete}
                data={item}
                key={index}
              />
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
