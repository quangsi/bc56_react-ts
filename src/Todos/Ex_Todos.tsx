import React, { useState } from "react";
import FormTodo from "./FormTodo";
import ListTodo from "./ListTodo";
import { I_Todo } from "./interface";

export default function Ex_Todos() {
  // <I_Todo[]> array chứa object
  const [todos, setTodos] = useState<I_Todo[]>([
    {
      id: 1,
      title: "Làm capstone nha",
      isCompleted: true,
    },
    {
      id: 2,
      title: "Làm dự án cuối khoá nha",
      isCompleted: false,
    },
  ]);
  let handleAddTodo = (newTodo: I_Todo) => {
    let newTodos = [...todos, newTodo];
    setTodos(newTodos);
  };
  let handleDelete = (idTodo: number) => {
    let newTodos = todos.filter((todo) => todo.id !== idTodo);
    setTodos(newTodos);
  };
  let handleToggleCheck = (idTodo: number) => {
    let index = todos.findIndex((item) => {
      return item.id === idTodo;
    });

    let newTodos = [...todos];
    newTodos[index].isCompleted = !newTodos[index].isCompleted;
    setTodos(newTodos);
  };
  return (
    <div className="container">
      <FormTodo handleAddTodo={handleAddTodo} />
      <ListTodo handleToggleCheck={handleToggleCheck} handleDelete={handleDelete} todos={todos} />
    </div>
  );
}
