import React, { useState } from "react";
import { I_PropsForm, I_Todo } from "./interface";
import { customAlphabet } from "nanoid";
export default function FormTodo({ handleAddTodo }: I_PropsForm) {
  const [title, setTitle] = useState<string>("");
  let handleChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  };

  let handleCreateTodo = () => {
    const nanoid = customAlphabet("123456789", 3);
    let newTodos: I_Todo = {
      id: parseInt(nanoid()),
      title: title,
      isCompleted: false,
    };
    handleAddTodo(newTodos);
  };
  return (
    <div className="d-flex align-items-center">
      <div className="form-group border border-danger w-100 m-0">
        <input
          onChange={handleChangeTitle}
          value={title}
          type="text"
          className="form-control"
        />
      </div>
      <button onClick={handleCreateTodo} className="btn btn-danger">
        Add
      </button>
    </div>
  );
}
