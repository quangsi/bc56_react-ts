// mô tả object todo

export interface I_Todo {
  id: number;
  title: string;
  isCompleted: boolean;
}

export interface I_PropsForm {
  handleAddTodo: (todo: I_Todo) => void;
}

export interface I_PropsList {
  todos: I_Todo[];
  handleDelete: (id: number) => void;
  handleToggleCheck: (id: number) => void;
}

export interface I_PropsItem {
  data: I_Todo;
  handleDelete: (id: number) => void;
  handleToggleCheck: (id: number) => void;
}
