import React from "react";
import { I_PropsItem } from "./interface";

export default function ItemTodo({ data, handleDelete, handleToggleCheck }: I_PropsItem) {
  return (
    <tr>
      <td>{data.id}</td>
      <td>{data.title}</td>
      <td>
        <input
          onChange={() => {
            handleToggleCheck(data.id);
          }}
          type="checkbox"
          checked={data.isCompleted}
        />
      </td>
      <td>
        <button
          onClick={() => {
            handleDelete(data.id);
          }}
          className="btn btn-danger"
        >
          Delete
        </button>
      </td>
    </tr>
  );
}
